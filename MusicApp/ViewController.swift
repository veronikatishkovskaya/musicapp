import UIKit
import AVKit

class ViewController: UIViewController {
    @IBOutlet weak var songsTableView: UITableView!
    @IBOutlet weak var songSlider: UISlider!
    @IBOutlet weak var playSong: UIButton!
    
    var player: AVPlayer?
    var playerItem: AVPlayerItem?
    var songsArray = [Song]()
    var isPlaying = Bool()
    var isSaved = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playSong.isEnabled = false
        isPlaying = false
        playSong.setImage(UIImage(named: "play"), for: .normal)
        configureAudioList()
        loadMusic()
    }
    
    @IBAction func pauseSong(_ sender: UIButton) {
        isPlaying = !isPlaying
        if isPlaying == true {
            player?.play()
            playSong.setImage(UIImage(named: "pause"), for: .normal)
        } else {
            player?.pause()
            playSong.setImage(UIImage(named: "play"), for: .normal)
        }
    }
    
    func configureAudioList() {
        songsArray.append(Song(name: "Thunderstruck",
                               album: "The Razor's Edge",
                               artist: "AC/DC",
                               url: "AC-DC - Thunderstruck",
                               isSaved: false))
        songsArray.append(Song(name: "Babushka Boi",
                               album: "Babushka Boi",
                               artist: "A$AP Rocky",
                               url: "ASAP Rocky - Babushka Boi",
                               isSaved: false))
        songsArray.append(Song(name: "Holiday",
                               album: "American Idiot",
                               artist: "Green Day",
                               url: "Green Day - Holiday",
                               isSaved: false))
        songsArray.append(Song(name: "Bandit",
                               album: "single",
                               artist: "Juice WRLD & YoungBoy",
                               url: "Juice WRLD & YoungBoy Never Broke Again - Bandit",
                               isSaved: false))
        songsArray.append(Song(name: "Industry Baby",
                               album: "single",
                               artist: "Lil Nas X feat. Jack Harlow",
                               url: "Lil Nas X feat. Jack Harlow - Industry Baby",
                               isSaved: false))
        songsArray.append(Song(name: "Slay3r",
                               album: "Whole Lotta Red",
                               artist: "Playboi Carti",
                               url: "Playboi Carti - Slay3r",
                               isSaved: false))
        songsArray.append(Song(name: "Only Us",
                               album: "single",
                               artist: "Sera",
                               url: "Sera - Only Us",
                               isSaved: false))
        songsArray.append(Song(name: "Not Sober",
                               album: "Over you",
                               artist: "The Kid Laroi feat. POLO G & Stunna Gambino",
                               url: "The Kid Laroi feat. POLO G & Stunna Gambino - Not Sober",
                               isSaved: false))
        songsArray.append(Song(name: "Holy Smokes",
                               album: "Trip at Knight",
                               artist: "Trippie Redd feat. Lil Uzi Vert",
                               url: "Trippie Redd feat. Lil Uzi Vert - Holy Smokes",
                               isSaved: false))
    }
    
    func playSong(url: String) {
        DispatchQueue.main.async {
            self.playSong.setImage(UIImage(named: "pause"), for: .normal)
            guard let url = Bundle.main.url(forResource: url, withExtension: "mp3") else { return }
            let playerItem = AVPlayerItem(url: url)
            self.player = AVPlayer(playerItem: playerItem)
            self.player?.play()
            self.setPeriodicTimeProgressBar()
        }
    }
    
    func saveMusic(indexPath: IndexPath) {
        songsArray[indexPath.row].isSaved = isSaved
        UserDefaults.standard.set(encodable: songsArray, forKey: Keys.songInfo.rawValue)
    }
    
    func loadMusic() {
        let savedSongs = Manager.shared.loadData()
        if savedSongs.count != 0 {
            songsArray = savedSongs
        }
    }
    
    func checkSavedSongs(cell: CustomTableViewCell, indexPath: IndexPath) {
        if let isSaved = songsArray[indexPath.row].isSaved {
            if isSaved == true {
                self.isSaved = true
            } else {
                self.isSaved = false
            }
        }
    }
    
    func setPeriodicTimeProgressBar() {
        DispatchQueue.main.async {
            self.player?.addPeriodicTimeObserver(forInterval: CMTimeMake(value: 1, timescale: 30), queue: .main) { time in
                let fraction = CMTimeGetSeconds(time) / CMTimeGetSeconds((self.player?.currentItem!.duration)!)
                self.songSlider.value = Float(fraction)
            }
        }
    }
    
    func setSelectedCellColor(cell: CustomTableViewCell) {
        let backgroundView = UIView()
        backgroundView.backgroundColor = #colorLiteral(red: 0.1960583925, green: 0.1960916221, blue: 0.1960479617, alpha: 1)
        cell.selectedBackgroundView = backgroundView
    }
    
    func songSavingAlert() {
        let alert = UIAlertController(title: "Play error",
                                      message: "To start playback, you need to download the file first",
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as? CustomTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(object: songsArray[indexPath.row])
        cell.isSaved = songsArray[indexPath.row].isSaved!
        cell.backgroundColor = view.backgroundColor
        setSelectedCellColor(cell: cell)
        checkSavedSongs(cell: cell, indexPath: indexPath)
        
        cell.downloadSongHandler = { sender in
            if self.songsArray[indexPath.row].isSaved == true {
                self.songsArray[indexPath.row].isSaved = false
                self.isSaved = false
                self.saveMusic(indexPath: indexPath)
            } else {
                self.songsArray[indexPath.row].isSaved = true
                self.isSaved = true
                self.saveMusic(indexPath: indexPath)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = storyboard?.instantiateViewController(withIdentifier: "AudioInfoViewController") as! AudioInfoViewController
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as? CustomTableViewCell else {return}
        
        let currentSong = songsArray[indexPath.row]
        controller.artist = currentSong.artist ?? ""
        controller.trackName = currentSong.name ?? ""
        controller.album = currentSong.album ?? ""
 
        if let song = currentSong.url {
            checkSavedSongs(cell: cell , indexPath: indexPath)
            if songsArray[indexPath.row].isSaved == true {
                playSong.isEnabled = true
                playSong(url: song)
                isPlaying = true
                self.navigationController?.pushViewController(controller, animated: true)
            } else {
               songSavingAlert()
            }
        }  
    }
}
