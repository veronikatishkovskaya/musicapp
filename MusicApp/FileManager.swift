import Foundation
import UIKit

enum Keys: String {
    case songInfo
}

class Manager {
    static let shared = Manager()
    
    func saveData(_ data: Song) {
        var songList = self.loadData()
        songList.append(data)
        UserDefaults.standard.setValue(songList, forKey: Keys.songInfo.rawValue)
    }
    
    func loadData() -> [Song] {
        guard let songs = UserDefaults.standard.value([Song].self, forKey: Keys.songInfo.rawValue) else {
            return []
        }
        return songs
    }  
}

