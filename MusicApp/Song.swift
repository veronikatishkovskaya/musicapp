import Foundation
import UIKit

class Song: Codable {
    var name: String?
    var album: String?
    var artist: String?
    var url: String?
    var isSaved: Bool?
    
    init(name: String?, album: String?, artist: String?, url: String?, isSaved: Bool?) {
        self.name = name
        self.album = album
        self.artist = artist
        self.url = url
        self.isSaved = isSaved
    }
    
    enum Keys: String, CodingKey {
        case name
        case album
        case artist
        case url
        case isSaved
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Keys.self)
        name = try container.decodeIfPresent(String.self, forKey: .name)
        album = try container.decodeIfPresent(String.self, forKey: .album)
        artist = try container.decodeIfPresent(String.self, forKey: .artist)
        url = try container.decodeIfPresent(String.self, forKey: .url)
        isSaved = try container.decodeIfPresent(Bool.self, forKey: .isSaved)
        
        
        
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Keys.self)
        try container.encode(self.name, forKey: .name)
        try container.encode(self.album, forKey: .album)
        try container.encode(self.artist, forKey: .artist)
        try container.encode(self.url, forKey: .url)
        try container.encode(self.isSaved, forKey: .isSaved)
    }
    
}

extension UserDefaults {
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
           let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}
