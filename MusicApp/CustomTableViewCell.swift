import UIKit

class CustomTableViewCell: UITableViewCell {
    @IBOutlet weak var trackName: UILabel!
    @IBOutlet weak var downloadStatus: UIButton!
    @IBOutlet weak var downloadProgress: UIProgressView!
    
    var isSaved = Bool()
    var progress = Progress(totalUnitCount: 5)
    var downloadSongHandler:((_ sender: AnyObject) -> Void)?
    
    func configure(object: Song) {
        guard let artist = object.artist else {return}
        guard let name = object.name else {return}
        guard let isSaved = object.isSaved else {return}
        trackName.text = "\(artist) - \(name)"
        if isSaved {
            self.downloadStatus.setImage(UIImage(named: "check"), for: .normal)
        } else {
            downloadStatus.setImage(UIImage(named: "download"), for: .normal)
        }
        downloadProgress.isHidden = true
    }
    
    @IBAction func downloadSong(_ sender: AnyObject) {
        isSaved = !isSaved
        if isSaved {
            downloadProgress.isHidden = false
            downloadStatus.isHidden = true
            self.startDownloading()
            
        } else {
            downloadStatus.setImage(UIImage(named: "download"), for: .normal)
        }
        if self.downloadSongHandler != nil {
            self.downloadSongHandler!(sender)
        }
    }
    
    func startDownloading() {
        self.downloadProgress.progress = 0.0
        self.progress.completedUnitCount = 0
        
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (timer) in
            guard self.progress.isFinished == false else {
                timer.invalidate()
                self.downloadProgress.isHidden = true
                if self.downloadProgress.isHidden {
                    self.downloadStatus.isHidden = false
                    self.downloadStatus.setImage(UIImage(named: "check"), for: .normal)
                }
                return
            }
            self.progress.completedUnitCount += 1
            self.downloadProgress.setProgress(Float(self.progress.fractionCompleted), animated: true)
        }
    }
}
