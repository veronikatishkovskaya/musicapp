import Foundation
import UIKit

class AudioInfoViewController: UIViewController {
    
    @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var albumLabel: UILabel!
    
    var artist = String()
    var trackName = String()
    var album = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureLabel()
        artistLabel.text = "Artist: \(artist)"
        nameLabel.text = "Name: \(trackName)"
        albumLabel.text = "Album: \(album)"
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    func configureLabel() {
        aboutLabel.layer.borderWidth = 1
        aboutLabel.layer.borderColor = CGColor(red: 255, green: 255, blue: 255, alpha: 1)
        aboutLabel.layer.cornerRadius = aboutLabel.frame.height / 2
    }
}
